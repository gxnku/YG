&emsp;

> # 连享会：因果推断专题

<img style="width: 450px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh-YG01.png">


&emsp;

## 0. 课程特色

因果推断在经验研究中的重要性无需多言。然而，面对快速涌现的各类方法，大家往往既兴奋又无助。

本次课程分为五个模块，力求帮助大家建立理解因果推断的基本框架，在深入理解基本概念和模型的基础上，能够合理地使用常用分析工具，自信地解读实证结果，并配合异质性分析、可视化图形、安慰剂检验等手段来增强实证分析部分的稳健性和可读性。

本次课程由两位授课经验丰富的老师精心设计，尤其是徐轶青老师，将他在加州大学和斯坦福大学的授课内容原汁原味地呈现出来。

在课程设计理念上，我们力求「讲慢、讲透」。与其给大家一堆「半生不熟」的方法和模型，倒不如把最核心的模型和方法讲透，以便赋予各位自我研读的能力和信心。为此，本次课程分三个周末讲授，以便大家有充足的时间理解、吸收和实操。我们强烈建议各位在课程期间将所学方法应用到你手头的论文中。我们安排了充足的答疑时间，并有 20 位经验丰富的助教随时协助大家自我提升。

&emsp;

## 1. 课程概况

- **嘉宾：** 司继春（上海对外经贸大学）；徐轶青（斯坦福大学）
- **模式：** 网络直播
- **时间：** 2021 年 12 月 (共五天)
  - 司继春老师：12 月 11-12 日
  - 徐轶青老师：12 月 18-19 日； 26 日
  - **时段：** 上午 9:00-12:00，下午 1:30-4:30，答疑：4:30-5:00
- **课程主页：** <https://gitee.com/lianxh/YG> 
  - &#x2B55; [PDF 课纲](https://file.lianxh.cn/KC/lianxh_YG.pdf)
  - &#x2B55; [课纲论文PDF打包下载](https://www.jianguoyun.com/p/DSZtQpAQ1bn4CBiW0JgE)
- **报名链接：** <http://junquan18903405450.mikecrm.com/Jjwxjbv>
- **助教招聘：** <https://www.wjx.top/vj/wFLeQ1w.aspx>


&emsp;

## 2. 主讲嘉宾简介

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/司继春-工作照-半身.png)

[司继春](http://www.suibe.edu.cn/txxy/b1/2d/c5782a45357/page.htm)，上海对外经贸大学统计与信息学院讲师，主要研究领域为微观计量经济学、产业组织理论。在 Journal of Business and Economic Statistics、《财经研究》等学术刊物上发表多篇论文。其实，大家更熟悉的是知乎上大名鼎鼎的 [**慧航**](https://www.zhihu.com/people/sijichun/columns)，拥有近 30 万个关注者，获得过 42w+ 次赞同，他就是司继春老师 —— [**知乎-慧航**](https://www.zhihu.com/people/sijichun/columns)。

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E5%BE%90%E8%BD%B6%E9%9D%92-%E5%B7%A5%E4%BD%9C%E7%85%A7%E5%8D%8A%E8%BA%AB%E7%85%A7.png)

[徐轶青](https://yiqingxu.org/)，斯坦福大学政治学系助理教授。主要研究领域为面板数据因果推断。本科毕业于复旦大学经济学系；硕士毕业于北京大学中国经济研究中心（现国家发展研究院）；2016 年于麻省理工学院（MIT）政治学系获博士学位。2016 年 7 月至 2019 年 6 月，于加州大学圣地亚哥分校（UCSD）政治学系工作。徐老师的工作成果发表于政治学国际顶级期刊，包括《美国政治学评论》（APSR）、《美国政治学期刊》（AJPS）、《政治学杂志》（JOP）、《政治分析》（PA）。他还获得了多个专业奖项，包括 John Williams 最佳博士论文选题奖（2014）、Malcolm Jewell 最佳研究生论文奖（2015）、AJPS 年度最佳论文奖（2016）、PA 年度编辑推荐论文奖（2017）、PA 年度最佳论文奖——The Miller Prize（2018、2020）。徐老师也是一个热爱 [教学](https://yiqingxu.org/teaching/) 的达人，他编写了多个被广泛应用的 [程序](https://yiqingxu.org/software/#panel-data-methods)。在 2019 年 8 月由美国西北大学和杜克大学联合举办的「因果推断 Workshops」中 ([main](https://web.law.duke.edu/conferences/2019/casualinference/main/), [advanced](https://web.law.duke.edu/conferences/2019/causalinference/advanced/))，徐老师应邀主讲「面板因果推断」专题 (本次课程的模块五)。他是此次 workshop 中最年轻的主讲嘉宾，其他嘉宾还包括：2021 年诺奖得主 Joshua Angrist 教授，Rubin 模型的提出者 Donald B. Rubin 教授，斯坦福大学的 Jens Hainmueller 教授等。

&emsp;

## 3. 课程详情

本课程第一阶段的内容由上海对外经贸大学的司继春老师讲解。司老师的数量经济科班背景加之知乎年度荣誉答主的身份，让他的授课既有极清晰的逻辑，又有深入浅出的解读。在过去的几年中，司老师已经为多所高校的讲授了因果推断和机器学习等专题课程，在学生中的口碑甚佳。 

第一阶段的课程主要包括两个模块 (两天)，详情如下：
- **模块一** 介绍本次课程各个模块的基础工具。从条件期望和因果图入手，从因果推断的视角重新解读线性回归、虚拟变量、交叉项的含义，并进而引入交叉验证、加权最小二乘法、局部回归等方法。
- **模块二** 力求以 Rubin 因果模型为基础，帮助大家建立起「反事实框架」。目前主流的因果推断方法基本上都以该模型为基础，比如 DID，RDD，PSM，SCM，Synthetic-DID，甚至是以及机器学习为基础的因果推断本质上都是估计「反事实」的过程。一旦完成从传统的线性回归向「反事实框架」的思维转变，就可以从容面对不断涌现的新方法了。

本课程第二阶段的内容由斯坦福大学的徐轶青老师讲解。过去几年，徐老师在美国高校给博士生讲授统计基础和因果推断课程。和他的斯坦福的教学方式类似，讲课将采取理论与实践相结合的方式，旨在帮助学员们从模型、直觉和操作三个方面掌握这些方法。

第二阶段的课程主要有三个模块（三天），具体说明如下：

- **模块三** 是第一阶段的延伸。着重介绍两个主题：如何度量不确定性？如何估计和展示不确定性？首先，我们将介绍几种常用的度量不确定性的方法、解释其适用场景，以及 Stata 实操。接着，徐老师将探讨常用的估计因果效应异质性的办法。这部分内容具有广泛的应用：不仅适用于实验数据，也适用于观察性数据；既适用于截面数据，又适用于面板数据。
- **模块四** 讲解截面数据常用的因果推断设计，包括工具变量设计和断点回师设计。徐老师将介绍两种研究方法的操作细节、如何阐释得到的结果、以及可能的误用。徐老师也将提供数据和 Stata 代码供学员们学习使用。
- **模块五** 重点介绍面板数据因果推断。首先，讨论常用的固定效应模型与倍差法，包括如何进行估计和推断，以及如何画动态因果效应图和进行安慰剂检验。之后，徐老师将介绍近年来这个领域的最新进展，包括对常用方法的反思和改进、合成控制法，以及一些基于机器学习工具的新估计方法。这也是他本人专长的研究领域。

&emsp; 

> ## 模块一：因果推断基础

### &emsp;**第 1 讲**&ensp;因果推断基础 I：拟合技巧

- 条件期望、因果图 (Causal Diagram: DAGs) 简介和辛普森悖论
- 线性回归和虚拟变量回归
- 作为拟合工具的线性回归：拟合优度和模型选择（R2、交叉验证、信息准则）
- 权重的使用：逆概率加权的思想、Horvitz-Thompson 估计量以及加权最小二乘
  - 实例：Wolfers, J. (2006). Did Unilateral Divorce Laws Raise Divorce Rates? A Reconciliation and New Results. **American Economic Review**, 96(5), 1802–1820. [-Link-data-](https://www.aeaweb.org/articles?id=10.1257/aer.96.5.1802), [-PDF-](<https://users.nber.org/~jwolfers/Papers/Divorce(AER).pdf>)
- 局部多项式估计及窗宽选取
  - 实例：Bronzini, R., & Iachini, E. (2014). Are Incentives for R&D Effective? Evidence from a Regression Discontinuity Approach. **American Economic Journal: Economic Policy**, 6(4), 100–134., [-Link-data-](https://www.aeaweb.org/articles?id=10.1257/pol.6.4.100), [-PDF-](https://sci-hub.ren/10.1257/pol.6.4.100)

### &emsp;**第 2 讲**&ensp;因果推断基础 II：作为解释的线性回归

- 回归结果的解释（模型设定方法）
  - 平方项
  - 交叉项
  - 虚拟变量
- 控制变量的选取
- 固定效应的使用
- 异质性与权重的使用

&emsp; 

> ## 模块二：因果推断理念和框架

### &emsp;**第 3 讲**&ensp;Rubin 因果模型和实验

- Rubin 因果模型 (Rubin Causal Model, RCM)
- 平均处理效应的分解：因果识别中的偏误
- 随机实验的计量经济学
  - 实例：Duflo, Esther, Rema Hanna, and Stephen P. Ryan. 2012. “Incentives Work: Getting Teachers to Come to School.” **American Economic Review** 102 (4): 1241–78. [-Link-data-](https://www.aeaweb.org/articles?id=10.1257/aer.102.4.1241), [-PDF-](http://sci-hub.ren/10.1257/AER.102.4.1241)
  - 实例（ITT）：OHIE 实验。
    - Finkelstein, A., et al. (2012). The Oregon Health Insurance Experiment: Evidence from the First Year*. **Quarterly Journal of Economics**, 127(3): 1057–1106.[-Link-data-](https://academic.oup.com/qje/article/127/3/1057/1923446), [-PDF-](https://sci-hub.ren/10.1093/qje/qjs020)
    - Taubman, S. L., Allen, H. L., Wright, B. J., Baicker, K., & Finkelstein, A. N. (2014). Medicaid Increases Emergency-Department Use: Evidence from Oregon’s Health Insurance Experiment. **Science**, 343(6168), 263–268. [-PDF-](https://www.science.org/doi/pdf/10.1126/science.1246183)

### &emsp;**第 4 讲**&ensp;无混淆分配下的因果推断：匹配 (3 小时)

- 无混淆分配假设
- 无混淆分配假设下的推断：匹配
- 匹配的 Stata 实现：Imbens ([2015](https://www.jianguoyun.com/p/De0KzkYQtKiFCBidyr8D)) 中的例子
- 倾向得分匹配的原理及实现
- 逆概率加权法、双向稳健的逆概率加权方法
- 扩展阅读：
  - Imbens, G. W., & Rubin, D. B. (2015). Causal inference for statistics, social, and biomedical sciences: An introduction. **Cambridge University Press**. [-PDF-](https://www.cambridge.org/core/books/causal-inference-for-statistics-social-and-biomedical-sciences/71126BE90C58F1A431FE9B2DD07938AB)


&emsp; 

> ## 模块三：实验设计的延伸

### &emsp;**第 5 讲**&ensp;度量不确定性

- 回顾实验方法
- Clustering standard errors
- Bootstrap methods
- Permutation Inference
- Stata 实操

### &emsp;**第 6 讲**&ensp;因果效应的异质性

- 异质性问题解析
- 估计边际效应（Marginal Effect）及其可视化
- Stata 实操
- 参考文献：
  - Bertrand, Marianne, Esther Duflo, and Sendhil Mullainathan. 2004. “How Much Should We Trust Differences-in-Differences Estimates?” **Quarterly Journal of Economics** 119 (1): 249–75. [-Link-](https://academic.oup.com/qje/article/119/1/249/1876068), [-PDF-](https://personal.utdallas.edu/~d.sul/Econo2/Marianne_etal_QJE_04.pdf)
  - Hainmueller, Jens, Jonathan Mummolo, and Yiqing Xu (徐轶青). 2019. “How Much Should We Trust Estimates from Multiplicative Interaction Models? Simple Tools to Improve Empirical Practice.” **Political Analysis**, 27 (2): 163-192. [-Link-](https://doi.org/10.1017/pan.2018.46), [-PDF-](https://www.cambridge.org/core/services/aop-cambridge-core/content/view/D8CAACB473F9B1EE256F43B38E458706/S1047198718000463a.pdf/div-class-title-how-much-should-we-trust-estimates-from-multiplicative-interaction-models-simple-tools-to-improve-empirical-practice-div.pdf)


&emsp; 

> ## 模块四：截面数据常用设计

### &emsp;**第 7 讲**&ensp;工具变量设计 (IV)

- 传统范式
- 新范式：Local Average Treatment Effect
- 实例：小班教学有没有用？
- 对使用工具变量的反思
- 参考文献：
  - Angrist, Joshua D., Guido W. Imbens, and Donald B. Rubin. 1996. “Identification of Causal Effects Using Instrumental Variables.” **Journal of the American Statistical Association** 91 (434): 444–55. [-Link-](), [-PDF-](http://www.jakebowers.org/ITVExperiments/angristimbensrubin96.pdf)
  - Angrist, J. D., and V. Lavy. 1999. “Using Maimonides’ Rule to Estimate the Effect of Class Size on Scholastic Achievement.” **Quarterly Journal of Economics** 114 (2): 533–75.

### &emsp;**第 8 讲**&ensp;断点回归设计 (RDD)
- 断点回归理论基础
- 断点回归一些细节问题
- 基于地理的断点回归
- 实例：秘鲁 Mita 的长期效应
  - Dell, M. 2010. “The Persistent Effects of Peru’s Mining Mita.” **Econometrica** 78 (6): 1863–1903. [-Link-](https://onlinelibrary.wiley.com/doi/abs/10.3982/ECTA8121), [-PDF-](https://sci-hub.ren/10.2307/2291629)


&emsp; 

> ## 模块五：面板数据因果推断

### &emsp;**第 9 讲**&ensp;固定效应模型与倍差法 (FD, DID)

- 估计与推断
  - 实例：Minimum Wage and Employment
  - Card, David, and Alan B. Krueger. 1994. “Minimum Wages and Employment: A Case Study of the Fast-Food Industry in New Jersey and Pennsylvania.” **American Economic Review** 84 (4): 772–93. [-PDF-](https://davidcard.berkeley.edu/papers/min-wage-ff-nj.pdf)
- 展示动态效应
  - 实例：Malaria Eradication in the Americas
  - Bleakley, Hoyt. 2010. “Malaria Eradication in the Americas: A Retrospective Analysis of Childhood Exposure.” **American Economic Journal: Applied Economics** 2 (2): 1-45. [-Link-data-](https://www.aeaweb.org/articles?id=10.1257/app.2.2.1), [-PDF-](https://sci-hub.ren/10.1257/APP.2.2.1)
- 安慰剂检验

### &emsp;**第 10 讲**&ensp;DID 前沿、合成控制法 (SCM)

- 对固定效应模型和倍差法的反思
- 合成控制法
  - 实例：California Proposition 99
  - Abadie, Alberto, Alexis Diamond, and Jens Hainmueller. 2010. “Synthetic Control Methods for Comparative Case Studies: Estimating the Effect of California’s Tobacco Control Program.” **Journal of the American Statistical Association** 105 (490): 493–505. [-PDF-](https://web.stanford.edu/~jhain/Paper/JASA2010.pdf)
- 对合成控制法的拓展
  - 实例：Election-Day Registration and Voter Turnout. 
  - Xu, Yiqing (徐轶青). 2017. “Generalized Synthetic Control Method: Causal Inference with Interactive Fixed Effects Models.” **Political Analysis** 25 (1): 57–76. [-PDF-](https://sci-hub.ren/10.1017/PAN.2016.2)
  

&emsp;

## 4. 报名信息以及缴费方式

### 4.1 报名信息

- **主办方：** 太原君泉教育咨询有限公司
- **标准费用**(含报名费、材料费)：
  - 全价：4500 元/人
  - 团报价 (三人及以上)/老学员（现场班/专题课学员）： 9 折，4050 元/人
  - **会员优惠：** 85 折，3825 元/人
- **Note：** 以上各项优惠不能叠加使用。
- **联系方式：**
  - 邮箱：[wjx004@sina.com](wjx004@sina.com)
  - 王老师：18903405450 (微信同号)
  - 李老师：‭18636102467 (微信同号)

> **报名链接：**  
>  [http://junquan18903405450.mikecrm.com/Jjwxjbv](http://junquan18903405450.mikecrm.com/Jjwxjbv)

**长按/扫描二维码报名：**

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/报名：因果推断.png)

### 4.2 缴费方式

> **方式 1：对公转账**

- 户名：太原君泉教育咨询有限公司
- 账号：35117530000023891 (晋商银行股份有限公司太原南中环支行)
- **温馨提示：** 对公转账时，请务必提供「**汇款人姓名-单位**」信息，以便确认。

> **方式 2：微信扫码支付**

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/君泉收款码.png)

> **温馨提示：** 微信转账时，请务必在「添加备注」栏填写「**汇款人姓名-单位**」信息。

&emsp;

## 5. 助教招聘

> ### 说明和要求

- **名额：** 10 名
- **任务：**
  - **A. 课前准备**：协助完成 3 篇推文，风格类似于 [lianxh.cn](https://www.lianxh.cn) 推文；
  - **B. 开课前答疑**：协助学员安装课件和软件，在微信群中回答一些常见问题；
  - **C. 上课期间答疑**：针对前一天学习的内容，在微信群中答疑 (8:00-9:00，19:00-22:00)；参见 [往期答疑](https://gitee.com/arlionn/PX/wikis/README.md?sort_id=3372005)。
  - Note: 下午 5:30-6:00 的课后答疑由主讲教师负责。
- **要求：** 热心、尽职，熟悉 Stata 的基本语法和常用命令，能对常见问题进行解答和记录。
- **特别说明：** 往期按期完成任务的助教自动获得本期助教资格，不必填写申请资料，直接联系连老师即可。
- **截止时间：** 2021 年 11 月 28 日 (将于 11 月 30 日公布遴选结果于连享会主页：[lianxh.cn](https://www.lianxh.cn))

> ### 申请链接：
>
> <https://www.wjx.top/vj/wFLeQ1w.aspx>

> 或 扫码在线填写助教申请资料：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/助教：因果推断.png)

> **课程主页：** <https://gitee.com/arlionn/YG>

&emsp;

> #### 关于我们

- **Stata 连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，500+ 推文，实证分析不再抓狂；[Bilibili 站](https://space.bilibili.com/546535876) 有视频大餐。
